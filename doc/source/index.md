# ATK

ATK is a java **Tango Application Toolkit** designed to build graphical application for the Tango control system.

```{toctree}
guide/atk
jdraw/jdraw
```
