package fr.esrf.tangoatk.widget.util.jdraw;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class JDRotatableLabel extends JDRectangular {

  static final Font fontDefault = new Font("Dialog", Font.PLAIN, 14);

  static BufferedImage img = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);
  private String theText;
  private Font theFont;

  private double angle;
  private double sAngle;
  private double sn;
  private double cs;
  private int    tx;
  private int    ty;

  private double minWidth;
  private double minHeight;
  private double minX;
  private double minY;
  private Polygon boundPolygon;
  private int[] px;
  private int[] py;

  /**
   * Construcxt a label.
   * @param objectName Name of this label
   * @param text Text (Single line only)
   * @param x Up left corner x coordinate
   * @param y Up left corner y coordinate
   */
  public JDRotatableLabel(String objectName, String text, int x, int y) {

    initDefault();
    setOrigin(new Point.Double(0.0, 0.0));
    summit = new Point.Double[8];
    name = objectName;
    theText = keepFirstLine(text);
    lineWidth = 0;
    angle = 0.0;
    createSummit();
    computeSummitCoordinates(x,y);
    updateShape();
    centerOrigin();

  }

  JDRotatableLabel(JDRotatableLabel e, int x, int y) {
    cloneObject(e, x, y);
    theText = new String(e.theText);
    theFont = new Font(e.theFont.getName(), e.theFont.getStyle(), e.theFont.getSize());
    angle = e.angle;
    updateShape();
  }


  // -----------------------------------------------------------
  // Overrides
  // -----------------------------------------------------------
  void initDefault() {
    super.initDefault();
    theText = "";
    theFont = fontDefault;
    angle = 0.0;
  }

  public boolean isInsideObject(int x, int y) {
    if (!super.isInsideObject(x, y)) return false;
    if(boundPolygon==null) {
      return true;
    } else {
      return boundPolygon.contains(x,y);
    }
  }

  public JDObject copy(int x, int y) {
    return new JDRotatableLabel(this, x, y);
  }


  public void paint(JDrawEditor parent,Graphics g) {

    if (!visible) return;
    Graphics2D g2 = (Graphics2D) g;
    prepareRendering(g2);
    super.paint(parent,g);

    // G2 initilialisation ----------------------------------

    g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
            RenderingHints.VALUE_FRACTIONALMETRICS_ON);

    g2.setFont(theFont);
    g2.setColor(foreground);
    FontRenderContext frc = g2.getFontRenderContext();
    int fa = (int) Math.ceil(g.getFont().getLineMetrics("ABC", frc).getAscent());


    // Rotation ----------------------------------------------------
    Shape oldClip = g2.getClip();
    g2.setClip(ptsx[0],ptsy[0],getWidth(),getHeight());
    g2.translate(ptsx[0]+tx,ptsy[0]+ty);
    g2.rotate(angle);


    // Draw Strings --------------------------------------------------
    g2.drawString(theText,1,1+fa);

    // Restore transform ---------------------------------------------
    g2.rotate(-angle);
    g2.translate(-ptsx[0]-tx,-ptsy[0]-ty);
    g2.setClip(oldClip);

  }

  void updateShape() {

    computeMinBounds();
    computeBoundRect();

    // Update shadow coordinates
    ptsx = new int[4];
    ptsy = new int[4];

    if (summit[0].x < summit[2].x) {
      if (summit[0].y < summit[6].y) {
        ptsx[0] = (int) summit[0].x;
        ptsy[0] = (int) summit[0].y;
        ptsx[1] = (int) summit[2].x + 1;
        ptsy[1] = (int) summit[2].y;
        ptsx[2] = (int) summit[4].x + 1;
        ptsy[2] = (int) summit[4].y + 1;
        ptsx[3] = (int) summit[6].x;
        ptsy[3] = (int) summit[6].y + 1;
      } else {
        ptsx[0] = (int) summit[6].x;
        ptsy[0] = (int) summit[6].y;
        ptsx[1] = (int) summit[4].x + 1;
        ptsy[1] = (int) summit[4].y;
        ptsx[2] = (int) summit[2].x + 1;
        ptsy[2] = (int) summit[2].y + 1;
        ptsx[3] = (int) summit[0].x;
        ptsy[3] = (int) summit[0].y + 1;
      }
    } else {
      if (summit[0].y < summit[6].y) {
        ptsx[0] = (int) summit[2].x;
        ptsy[0] = (int) summit[2].y;
        ptsx[1] = (int) summit[0].x + 1;
        ptsy[1] = (int) summit[0].y;
        ptsx[2] = (int) summit[6].x + 1;
        ptsy[2] = (int) summit[6].y + 1;
        ptsx[3] = (int) summit[4].x;
        ptsy[3] = (int) summit[4].y + 1;
      } else {
        ptsx[0] = (int) summit[4].x;
        ptsy[0] = (int) summit[4].y;
        ptsx[1] = (int) summit[6].x + 1;
        ptsy[1] = (int) summit[6].y;
        ptsx[2] = (int) summit[0].x + 1;
        ptsy[2] = (int) summit[0].y + 1;
        ptsx[3] = (int) summit[2].x;
        ptsy[3] = (int) summit[2].y + 1;
      }
    }

    if (hasShadow()) {
      computeShadow(true);
      computeShadowColors();
    }

    tx = (int)(-minX + (getWidth() - minWidth)/2.0);
    ty = (int)(-minY + (getHeight() - minHeight)/2.0);
    int[] bx = new int[4];
    int[] by = new int[4];
    for(int i=0;i<4;i++) {
      bx[i] = px[i]+ptsx[0]+tx;
      by[i] = py[i]+ptsy[0]+ty;
    }
    boundPolygon = new Polygon(bx,by,4);

  }

  // -----------------------------------------------------------
  // Property stuff
  // -----------------------------------------------------------
  /**
   * Sets the Font of this label.
   * @param f Font
   */
  public void setFont(Font f) {
    setFont(f,false);
  }

  /**
   * Sets the font of this label and resize it if needed and specified.
   * @param f Font
   * @param resize true to resize label when text is out of bounds.
   */
  public void setFont(Font f,boolean resize) {
    theFont = f;
    updateLabel(resize);
  }

  /**
   * @return the current font of this label.
   */
  public Font getFont() {
    return theFont;
  }

  /**
   * Sets the text rotation angle.
   */
  public void setAngle(double a) {
    angle = a;
    updateLabel(true);
  }

  /**
   * @return the current text rotation angle.
   * @see #setAngle
   */
  public double getAngle() {
    return angle;
  }

  /**
   * Sets the text of this label.
   * @param s Text value
   */
  public void setText(String s) {
    setText(s,false);
  }

  /**
   * Sets the text of this label and resize label if desried.
   * @param s Text value
   * @param resize true to resize label when text is out of bounds.
   */
  public void setText(String s,boolean resize) {
    theText = s;
    updateLabel(resize);
  }

  /**
   * @return the current label text.
   */
  public String getText() {
    return theText;
  }


  // -----------------------------------------------------------
  // File management
  // -----------------------------------------------------------
  void recordObject(StringBuffer to_write, int level) {

    StringBuffer decal = recordObjectHeader(to_write, level);

    if (theFont.getName() != fontDefault.getName() ||
            theFont.getStyle() != fontDefault.getStyle() ||
            theFont.getSize() != fontDefault.getSize()) {
      to_write.append(decal).append("font:\"");
      to_write.append(theFont.getName()).append("\",");
      to_write.append(theFont.getStyle()).append(",");
      to_write.append(theFont.getSize()).append("\n");
    }

    if (!theText.equals("")) {
      int i;
      String lines[] = theText.split("\n");
      to_write.append(decal).append("text:");
      for (i = 0; i < lines.length; i++) {
        if (i == 0) {
          to_write.append("\"").append(lines[i]).append("\"");
        } else {
          to_write.append(decal).append("     \"").append(lines[i]).append("\"");
        }
        if (i == lines.length - 1) {
          to_write.append("\n");
        } else {
          to_write.append(",\n");
        }
      }
    }

    if (angle != 0.0) {
      to_write.append(decal).append("textAngle:").append(angle).append("\n");
    }

    closeObjectHeader(to_write, level);

  }

  JDRotatableLabel(JDFileLoader f) throws IOException {

    initDefault();
    f.startBlock();
    summit = f.parseRectangularSummitArray();

    while (!f.isEndBlock()) {
      String propName = f.parseProperyName();
      if (propName.equals("text")) {
        theText = f.parseStringArray();
      } else if (propName.equals("textAngle")) {
        angle = f.parseDouble();
      } else if (propName.equals("font")) {
        theFont = f.parseFont();
      } else
        loadDefaultPropery(f, propName);
    }

    f.endBlock();

    updateShape();
  }

  // -----------------------------------------------------------
  // Undo buffer
  // -----------------------------------------------------------
  UndoPattern getUndoPattern() {

    UndoPattern u = new UndoPattern(UndoPattern._JDRotatableLabel);
    fillUndoPattern(u);
    u.fName = theFont.getName();
    u.fStyle = theFont.getStyle();
    u.fSize = theFont.getSize();
    u.angle = angle;
    u.text = new String(theText);

    return u;
  }

  JDRotatableLabel(UndoPattern e) {
    initDefault();
    applyUndoPattern(e);
    theFont = new Font(e.fName,e.fStyle,e.fSize);
    angle = e.angle;
    theText = e.text;
    updateShape();
  }

  // -----------------------------------------------------------
  // Private stuff
  // -----------------------------------------------------------

  private String keepFirstLine(String text) {

    int i = text.indexOf('\n');
    if(i!=-1)
      return text.substring(0,i);
    return text;

  }


  // Compute summit coordinates from width, height
  // 0 1 2
  // 7   3
  // 6 5 4
  private void computeSummitCoordinates(int x,int y) {

    computeMinBounds();

    // Compute summit

    summit[0].x = x;
    summit[0].y = y;

    summit[2].x = x + minWidth;
    summit[2].y = y;

    summit[4].x = x + minWidth;
    summit[4].y = y + minHeight;

    summit[6].x = x;
    summit[6].y = y + minHeight;

    centerSummit();

  }

  private void computeMinBounds() {

    // Measure text box
    Graphics g = img.getGraphics();
    g.setFont(theFont);
    Graphics2D g2 = (Graphics2D) g;
    g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
            RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
            RenderingHints.VALUE_FRACTIONALMETRICS_ON);
    FontRenderContext frc = g2.getFontRenderContext();
    Rectangle2D bounds = g.getFont().getStringBounds(theText, frc);
    double textWidth = (int) Math.ceil(bounds.getWidth()) + 2;
    double textHeight = (int) Math.ceil(bounds.getHeight()) + 2;

    // Rotate box
    sn = Math.sin(angle);
    cs = Math.cos(angle);

    minX = 0;
    minY = 0;
    double maxX = 0;
    double maxY = 0;
    Point2D.Double p1 = rotate(textWidth,0);
    minX = Math.min(minX,p1.x);
    maxX = Math.max(maxX,p1.x);
    minY = Math.min(minY,p1.y);
    maxY = Math.max(maxY,p1.y);
    Point2D.Double p2 = rotate(textWidth, textHeight);
    minX = Math.min(minX,p2.x);
    maxX = Math.max(maxX,p2.x);
    minY = Math.min(minY,p2.y);
    maxY = Math.max(maxY,p2.y);
    Point2D.Double p3 = rotate(0, textHeight);
    minX = Math.min(minX,p3.x);
    maxX = Math.max(maxX,p3.x);
    minY = Math.min(minY,p3.y);
    maxY = Math.max(maxY,p3.y);

    px = new int[4];
    py = new int[4];
    px[0] = 0;
    px[1] = (int)p1.x;
    px[2] = (int)p2.x;
    px[3] = (int)p3.x;
    py[0] = 0;
    py[1] = (int)p1.y;
    py[2] = (int)p2.y;
    py[3] = (int)p3.y;

    minWidth = maxX-minX;
    minHeight = maxY-minY;

  }

  private Point2D.Double rotate(double x,double y) {
    Point2D.Double r = new Point2D.Double();
    r.x = cs * x - sn * y;
    r.y = sn * x + cs * y;
    return r;
  }

  private void updateLabel(boolean resize) {
    if (resize) {
      if(getWidth()<=minWidth || getHeight()<=minHeight) {
        // Need resize
        // System.out.println("Resize label");
        double x = summit[0].x + minWidth;
        double y = summit[0].y + minHeight;
        summit[2].x = x;
        summit[4].x = x;
        summit[4].y = y;
        summit[6].y = y;
        centerSummit();
      }
    }
    updateShape();
  }

  public void rotate90(double x,double y) {
    super.rotate90(x,y);
    angle += Math.toRadians(90);
    updateShape();
  }

  public void restoreTransform() {
    angle = sAngle;
    updateShape();
    super.restoreTransform();
  }

  public void saveTransform() {
    sAngle = angle;
    updateShape();
    super.saveTransform();
  }

}
