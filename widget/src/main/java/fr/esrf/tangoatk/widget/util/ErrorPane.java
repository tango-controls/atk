/*
 *  Copyright (C) :	2002,2003,2004,2005,2006,2007,2008,2009
 *			European Synchrotron Radiation Facility
 *			BP 220, Grenoble 38043
 *			FRANCE
 * 
 *  This file is part of Tango.
 * 
 *  Tango is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Tango is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Tango.  If not, see <http://www.gnu.org/licenses/>.
 */
 
package fr.esrf.tangoatk.widget.util;

import fr.esrf.tangoatk.core.ATKException;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.command.CommandFactory;
import fr.esrf.Tango.DevFailed;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/** A tango error message popup with a 'detail' (show error stack) button. */
public class ErrorPane {

  static Image errorImage=null;

  private static ErrorDialog createDialog(Component parentComponent, ATKException e, String title,String devName) {

    Window window = ATKGraphicsUtils.getWindowForComponent(parentComponent);
    ErrorDialog ret;

    if (errorImage == null) {
      try {
        Class theClass = Class.forName("fr.esrf.tangoatk.widget.util.ErrorPane");
        errorImage = ImageIO.read(theClass.getResource("/fr/esrf/tangoatk/widget/util/error.gif"));
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }

    if( window==null ) {
      ret = new ErrorDialog((Frame)null, e,title,devName);
    } else {
      if (window instanceof Frame) {
        ret = new ErrorDialog((Frame)window, e,title,devName);
      } else {
        ret = new ErrorDialog((Dialog)window, e,title,devName);
      }
    }


    return ret;

  }

  /**
   * Show an Error Popup dialog.
   * @param parentComponent Parent component.
   * @param devName Device name that has triggered the error.
   * @param e The error.
   */
  public static void showErrorMessage(Component parentComponent,String devName,ATKException e) {

    showErrorMessage(parentComponent,"Error",devName,e);

  }

  /**
   * Show an Error Popup dialog.
   * @param parentComponent Parent component.
   * @param title Dialog title.
   * @param devName Device name that has triggered the error.
   * @param e The error.
   */
  public static void showErrorMessage(Component parentComponent,String title,String devName,ATKException e) {

    java.awt.EventQueue.invokeLater(() -> {
      ErrorDialog dlg = createDialog(parentComponent, e, title, devName);
      ATKGraphicsUtils.centerDialog(dlg);
      dlg.setVisible(true);
    });

  }

  /**
   * Show an Error Popup dialog.
   * @param parentComponent Parent component.
   * @param devName Device name that has triggered the error.
   * @param e The error.
   */
  public static void showErrorMessage(Component parentComponent,String devName,DevFailed e) {
    ATKException ae = new ATKException(e);
    showErrorMessage(parentComponent,"Error",devName,ae);
  }

  /**
   * Show an Error Popup dialog.
   * @param parentComponent Parent component.
   * @param title Dialog title.
   * @param devName Device name that has triggered the error.
   * @param e The error.
   */
  public static void showErrorMessage(Component parentComponent,String title,String devName,DevFailed e) {
    ATKException ae = new ATKException(e);
    showErrorMessage(parentComponent,title,devName,ae);
  }

  /**
   * Show an Error Popup dialog.
   * @param parentComponent Parent component.
   * @param title Dialog title.
   * @param devName Device name that has triggered the error.
   * @param e The error.
   */
  public static void showErrorMessage(Component parentComponent,String title,String devName,Exception e) {
    ATKException ae;
    if(e instanceof DevFailed)
      ae = new ATKException((DevFailed)e);
    else
      ae = new ATKException(e);
    showErrorMessage(parentComponent,title,devName,ae);
  }

  /**
   * Show an Error Popup dialog.
   * @param parentComponent Parent component.
   * @param devName Device name that has triggered the error.
   * @param e The error.
   */
  public static void showErrorMessage(Component parentComponent,String devName,Exception e) {
    showErrorMessage(parentComponent,"Error",devName,e);
  }

  public static void main(String[] args) {

    ATKException ee1 = new ATKException("This is an error");
    ErrorPane.showErrorMessage(null,"My error","",ee1);

    String longError = "This is a long ";
    for(int i=0;i<100;i++) longError+=" error";
    ATKException ee2 = new ATKException(longError);
    ErrorPane.showErrorMessage(null,"My error","",ee2);

    longError+="\nL2 error\nL3 Error";
    ATKException ee3 = new ATKException(longError);
    ErrorPane.showErrorMessage(null,"My error","",ee3);

    String mlError = "";
    for(int i=0;i<100;i++) mlError += Integer.toString(i)+" This is a multiline line error\n";
    ATKException ee4 = new ATKException(mlError);
    ErrorPane.showErrorMessage(null,"My error","",ee4);

    try {
      CommandFactory.getInstance().getCommand("jlp/test/1/Onn");
    } catch( ConnectionException e1) {
      ErrorPane.showErrorMessage(null,"My error","jlp/test/1",e1);
    } catch ( DevFailed e2) {
      ErrorPane.showErrorMessage(null,"My error","jlp/test/1",e2);
    } catch (Exception e3) {
      ErrorPane.showErrorMessage(null,"My error",null,new ATKException(e3));
    }
    System.exit(0);

  }

}

// --------------------------------------------------------------------------------
// Custom error dialog.
// --------------------------------------------------------------------------------
class ErrorDialog extends JDialog {

  JScrollPane simpleErrorView;
  JTextArea simpleError;
  JPanel simpleErrorPanel;
  JPanel buttonPanel;
  JButton okButton;
  JButton moreButton;
  JLabel iconLabel;
  ATKException theError;

  JTabbedPane tabPanel;
  ErrorTree stackPanel;
  JScrollPane stackView;

  JTextArea stackTraceText;
  JScrollPane stackTraceView;

  String deviceName;
  boolean showDetails=false;

  ErrorDialog(Frame parent,ATKException e,String title,String devName) {
    super(parent,title,true);
    theError = e;
    deviceName = devName;
    initComponents();
  }

  ErrorDialog(Dialog parent,ATKException e,String title,String devName) {
    super(parent,title,true);
    theError = e;
    deviceName = devName;
    initComponents();
  }

  private void fillInStackTrace() {

    StackTraceElement[] st = theError.getStackTrace();
    StringBuffer str = new StringBuffer();
    str.append(theError.getSourceName()+"\n at\n");
    for(int i=0;i<st.length;i++) {
      str.append(st[i]);
      str.append('\n');
    }
    stackTraceText.setText(str.toString());

  }

  private void maxSizeComponent(JComponent comp,int maxWidth,int maxHeight) {

    Dimension d = comp.getSize();
    boolean dChange = false;
    if(d.width>maxWidth) {d.width=maxWidth;dChange=true;}
    if(d.height>maxHeight) {d.height=maxHeight;dChange=true;}
    if(dChange) comp.setPreferredSize(d);

  }

  private void showDetails() {

    moreButton.setText("Hide...");
    showDetails = true;
    tabPanel.setVisible(true);
    pack();
    maxSizeComponent(simpleErrorView,800,600);
    maxSizeComponent(stackTraceView,800,400);
    if( theError.getStackLength()>0 ) maxSizeComponent(stackView,800,400);
    ATKGraphicsUtils.centerDialog(this);

  }

  private void hideDetails() {

    moreButton.setText("Details...");
    showDetails = false;
    tabPanel.setVisible(false);
    pack();
    maxSizeComponent(simpleErrorView,800,600);
    ATKGraphicsUtils.centerDialog(this);

  }

  private void initComponents() {

    Container contentPane = getContentPane();
    contentPane.setLayout(new GridBagLayout());

    okButton = new JButton("Ok");
    moreButton = new JButton("Details...");

    moreButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if( !showDetails ) {
          showDetails();
        } else {
          hideDetails();
        }
      }
    });

    okButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
        setVisible(false);
      }
    });

    simpleError = new JTextArea();
    simpleError.setEditable(false);
    simpleError.setBackground(contentPane.getBackground());
    simpleError.setFont(okButton.getFont());
    simpleErrorView = new JScrollPane(simpleError);
    simpleErrorView.setBorder(null);
    if( deviceName!=null && deviceName.length()>0 )
      simpleError.setText(deviceName + " :\n" + theError.getDescription());
    else
      simpleError.setText(theError.getDescription());

    simpleErrorPanel= new JPanel();
    simpleErrorPanel.setLayout(new GridBagLayout());
    iconLabel = new JLabel(new ImageIcon(ErrorPane.errorImage));
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill = GridBagConstraints.BOTH;
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.insets.top = 12;
    gbc.insets.bottom = 12;
    gbc.insets.left = 10;
    gbc.insets.right = 10;
    simpleErrorPanel.add(iconLabel,gbc);
    gbc.fill = GridBagConstraints.BOTH;
    gbc.insets.top = 2;
    gbc.insets.bottom = 2;
    gbc.insets.left = 2;
    gbc.insets.right = 10;
    gbc.gridx = 1;
    gbc.gridy = 0;
    gbc.weightx = 1;
    gbc.weighty = 1;
    gbc.ipadx = 10;
    simpleErrorPanel.add(simpleErrorView,gbc);

    buttonPanel = new JPanel();
    buttonPanel.setLayout(new FlowLayout());
    buttonPanel.setPreferredSize(new Dimension(260,40));
    buttonPanel.add(okButton);
    buttonPanel.add(moreButton);

    if( theError.getStackLength()>0 ) {
      stackPanel = new ErrorTree();
      stackPanel.addErrors(theError.getErrors());
      stackView = new JScrollPane(stackPanel);
      stackView.setBorder(BorderFactory.createEtchedBorder());
    }

    stackTraceText = new JTextArea();
    stackTraceText.setEditable(false);
    stackTraceView = new JScrollPane(stackTraceText);
    stackTraceView.setBorder(BorderFactory.createEtchedBorder());

    tabPanel = new JTabbedPane();
    if( theError.getStackLength()>0 )
      tabPanel.add("Error",stackView);
    tabPanel.add("Trace",stackTraceView);

    gbc.fill = GridBagConstraints.BOTH;
    gbc.insets.top = 0;
    gbc.insets.bottom = 0;
    gbc.insets.left = 0;
    gbc.insets.right = 0;
    gbc.ipadx = 0;
    gbc.ipady = 0;
    gbc.weightx = 1.0;
    gbc.weighty = 1.0;

    gbc.gridx = 0;
    gbc.gridy = 0;
    contentPane.add(simpleErrorPanel,gbc);
    gbc.gridx = 0;
    gbc.gridy = 1;
    contentPane.add(tabPanel,gbc);
    gbc.gridx = 0;
    gbc.gridy = 2;
    gbc.weighty = 0;
    contentPane.add(buttonPanel,gbc);

    fillInStackTrace();
    tabPanel.setVisible(false);

    pack();
    maxSizeComponent(simpleErrorView,800,600);
    maxSizeComponent(stackTraceView,800,400);
    if( theError.getStackLength()>0 ) maxSizeComponent(stackView,800,400);

  }

}

