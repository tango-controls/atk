/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.esrf.tangoatk.widget.util;

import java.io.File;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

/**
 *
 * @author poncet
 */
public class PreviewFileChooser extends JFileChooser
{
    private String                    rootDirectory = null;
    private boolean                   contentVisible = false;
    private FileContentViewer         fcv = new FileContentViewer(400, 250);
    
    private JTable                    fileChoosertable = null;
    
    private java.awt.Dimension        increaseDimension = new java.awt.Dimension(220, 70);
    
    public PreviewFileChooser ()
    {
        super();
        
        // Display the file list in details file list mode and sort the list according to
        // the last modified date.
        ActionMap am=this.getActionMap();
        Action details = am.get("viewTypeDetails");
        details.actionPerformed(null);
        
        fileChoosertable = SwingUtils.getDescendantsOfType(JTable.class, this).get(0);
        sortByDate();
        
        validate();
        increaseSize();
        
//        System.out.println("coucou");
    }
    
    public PreviewFileChooser (String dirPath)
    {
        super(dirPath);
        rootDirectory = dirPath;
        
        // Display the file list in details file list mode and sort the list according to
        // the last modified date.
        ActionMap am=this.getActionMap();
        Action details = am.get("viewTypeDetails");
        details.actionPerformed(null);
        
        fileChoosertable = SwingUtils.getDescendantsOfType(JTable.class, this).get(0);
        sortByDate();
        
        validate();
        increaseSize();
        
    }
    
    void sortByDate()
    {
        if (fileChoosertable == null) return;
        SwingUtilities.invokeLater( new Runnable()
                                        {
                                           @Override
                                           public void run()
                                            {
                                                fileChoosertable.getRowSorter().toggleSortOrder(2);
                                                fileChoosertable.getRowSorter().toggleSortOrder(2);
                                            }
                                        }
                                   );        
    }
    
    public void increaseDimension(int w, int h)
    {
        increaseDimension = new java.awt.Dimension(w, h);
        increaseSize();
    }
    
    private void increaseSize()
    {
        if (increaseDimension == null) return;
        java.awt.Dimension  psize = this.getPreferredSize();
        psize.width = psize.width + increaseDimension.width;
        psize.height = psize.height + increaseDimension.height;
        this.setPreferredSize(psize);
    }

    
    public String getRootDirectory()
    {
        return(rootDirectory);
    }
     
    public void setRootDirectory(String dirPath)
    {
        File dir = new File(dirPath);
        super.setCurrentDirectory(dir);
        rootDirectory = dirPath;
    }
   
    public boolean isContentVisible()
    {
        return contentVisible;
    }
    
    public void setContentVisible(boolean cv)
    {
        if (cv == isContentVisible())
            return;
        
        if (isContentVisible())
        {
            this.removePropertyChangeListener(fcv);
            this.setAccessory(null);
        }
        else
        {
            this.addPropertyChangeListener(fcv);
            this.setAccessory(fcv);
        }
        validate();
        increaseSize();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        final String folder;
        if (args.length > 0 )
            folder = args[0];
        else
            folder = "/home/esrf/poncet";
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                JFrame jf = new JFrame();
                jf.setVisible(true);
                
                PreviewFileChooser pjfc = new PreviewFileChooser(folder);
                pjfc.setContentVisible(true);
                
                int returnDial = pjfc.showDialog(jf.getRootPane(), "Load");
                if (returnDial == JFileChooser.CANCEL_OPTION)
                    return;
                
                java.io.File selectedFile = pjfc.getSelectedFile();
                if (selectedFile == null) return;

                if (!selectedFile.exists())
                {
                    JOptionPane.showMessageDialog(
                            jf.getRootPane(), "The selected file does not exist.\n\n",
                            "Load file aborted.\n",
                            javax.swing.JOptionPane.ERROR_MESSAGE);
                    return;
                }
                // do load action
                System.out.println("File to load : "+selectedFile.getName());
            }
        });
    }
}
