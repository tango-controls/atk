/*
 *  Copyright (C) :	2002,2003,2004,2005,2006,2007,2008,2009, 2024
 *			European Synchrotron Radiation Facility
 *			BP 220, Grenoble 38043
 *			FRANCE
 * 
 *  This file is part of Tango.
 * 
 *  Tango is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Tango is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Tango.  If not, see <http://www.gnu.org/licenses/>.
 */
// File:          BooleanScalarTickViewer.java
// Created:       2024-03-20 15:35:00, poncet
// By:            <poncet@esrf.fr>
//
//
// Description:
package fr.esrf.tangoatk.widget.attribute;

//import javax.swing.*;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.BooleanScalarEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IBooleanScalar;
import fr.esrf.tangoatk.core.IBooleanScalarListener;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.widget.util.jdraw.JDrawable;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

//import java.awt.Color;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//
//import fr.esrf.tangoatk.core.*;
//import fr.esrf.tangoatk.widget.util.ATKConstant;
//import fr.esrf.tangoatk.widget.util.jdraw.JDrawable;

/**
 * An BooleanScalarCheckBoxViewer is a BooleanScalar attribute viewer. This
 * means that the attribute used as the model for this viewer should implement
 * the IBooleanScalar interface. The viewer is updated when the boolean
 * attribute value changes. The checkBox is "checked" if the attribute value is
 * "true" and it is unchecked if the attribute value is "false".
 *
 */
public class BooleanScalarTickViewer extends JLabel
                                                 implements IBooleanScalarListener, JDrawable
{
    private static final String     CHECK = "\u2714";
    private static final String     CROSS = "\u2715";
    private static final String     NO_VALUE="?";
    
    private Color                   trueBackground = Color.white;
    private Color                   falseBackground = Color.lightGray;

    private IBooleanScalar        attModel = null;
    private boolean                hasToolTip = false;

    static String[] exts =
    {
        "trueBackground",
        "falseBackground"
    };

    // ---------------------------------------------------
    // Contruction
    // ---------------------------------------------------
    public BooleanScalarTickViewer()
    {
        super();
        setFont(new Font("Dialog", Font.BOLD, 20));
        setOpaque(true);
        setText(NO_VALUE);
    }


    // ---------------------------------------------------
    // Property stuff
    // ---------------------------------------------------
    public IBooleanScalar getAttModel()
    {
        return attModel;
    }
    
    public void clearModel()
    {
        if (attModel == null) return;
        attModel.removeBooleanScalarListener(this);
        attModel = null;
        setText(NO_VALUE);
    }

    public void setAttModel(IBooleanScalar boolModel)
    {
        clearModel();

        if (boolModel != null)
        {
            attModel = boolModel;
            attModel.addBooleanScalarListener(this);

            if (hasToolTip)
            {
                setToolTipText(boolModel.getName());
            }
            attModel.refresh();
        }
    }
    
    
    public Color getTrueBackground()
    {
        return trueBackground;
    }
    
    public void setTrueBackground(Color c)
    {
        trueBackground = c;
    }
    
    public Color getFalseBackground()
    {
        return falseBackground;
    }
    
    public void setFalseBackground(Color c)
    {
        falseBackground = c;
    }


    /**
     * <code>getHasToolTip</code> returns true if the viewer has a tooltip
     * (attribute full name)
     *
     * @return a <code>boolean</code> value
     */
    public boolean getHasToolTip()
    {
        return hasToolTip;
    }

    /**
     * <code>setHasToolTip</code> display or not a tooltip for this viewer
     *
     * @param b If True the attribute full name will be displayed as tooltip for
     * the viewer
     */
    public void setHasToolTip(boolean b)
    {
        if (hasToolTip != b)
        {
            if (b == false)
            {
                setToolTipText(null);
            }
            else
            {
                if (attModel != null)
                {
                    setToolTipText(attModel.getName());
                }
            }
            hasToolTip = b;
        }
    }



    // ---------------------------------------------------
    // JDrawable implementation
    // ---------------------------------------------------
    @Override
    public void initForEditing()
    {
        setText(NO_VALUE);
    }

    @Override
    public JComponent getComponent()
    {
        return this;
    }

    @Override
    public String getDescription(String extName)
    {
        if (extName.equalsIgnoreCase("trueBackground"))
        {
             return "Sets the TRUE background color (r,g,b) applied when the attriute boolean value is TRUE.\n";
        }
        if (extName.equalsIgnoreCase("falseBackground Color"))
        {
             return "Sets the FALSE background color (r,g,b) applied when the attriute boolean value is FALSE.\n";
        }
        return "";
    }

    @Override
    public String[] getExtensionList()
    {
        return exts;
    }
    
    private void showJdrawError(boolean popup, String paramName, String message)
    {
        if (popup)
        {
            JOptionPane.showMessageDialog(null, "BooleanScalarTickViewer: " + paramName + " incorrect.\n" + message,
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public boolean setExtendedParam(String name, String value, boolean popupAllowed)
    {
        
        // trueBackground
        if (name.equalsIgnoreCase("trueBackground"))
        {
            String[] c = value.split(",");
            if (c.length != 3)
            {
                showJdrawError(popupAllowed, "trueBackground", "Integer list expected: r,g,b");
                return false;
            }
            try
            {
                int r = Integer.parseInt(c[0]);
                int g = Integer.parseInt(c[1]);
                int b = Integer.parseInt(c[2]);
                if (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255)
                {
                    showJdrawError(popupAllowed, "trueBackground", "Parameter out of bounds. [0..255]");
                    return false;
                }
                trueBackground = new Color(r, g, b);
                return true;
            }
            catch (NumberFormatException e)
            {
                showJdrawError(popupAllowed, "trueBackground", "Wrong integer syntax.");
                return false;
            }
        }
               
        // trueBackground
        if (name.equalsIgnoreCase("falseBackground"))
        {
            String[] c = value.split(",");
            if (c.length != 3)
            {
                showJdrawError(popupAllowed, "falseBackground", "Integer list expected: r,g,b");
                return false;
            }
            try
            {
                int r = Integer.parseInt(c[0]);
                int g = Integer.parseInt(c[1]);
                int b = Integer.parseInt(c[2]);
                if (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255)
                {
                    showJdrawError(popupAllowed, "falseBackground", "Parameter out of bounds. [0..255]");
                    return false;
                }
                falseBackground = new Color(r, g, b);
                return true;
            }
            catch (NumberFormatException e)
            {
                showJdrawError(popupAllowed, "falseBackground", "Wrong integer syntax.");
                return false;
            }
        }
        return false;
    }


    @Override
    public String getExtendedParam(String name)
    {
        if (name.equalsIgnoreCase("trueBackground"))
        {
            Color c = trueBackground;
            return c.getRed() + "," + c.getGreen() + "," + c.getBlue();
        }
        if (name.equalsIgnoreCase("falseBackground"))
        {
            Color c = falseBackground;
            return c.getRed() + "," + c.getGreen() + "," + c.getBlue();
        }
        return "";
    }

    private void setBoolValue(boolean val )
    {
        if (val)
        {
            if (!getText().equalsIgnoreCase(CHECK))
                setText(CHECK);
            if (!getBackground().equals(trueBackground))
                setBackground(trueBackground);
        }
        else
        {
            if (!getText().equalsIgnoreCase(CROSS))
                setText(CROSS);
            if (!getBackground().equals(falseBackground))
                setBackground(falseBackground);
        }
    }

    // ---------------------------------------------------
    // IBooleanScalarListener listener
    // ---------------------------------------------------
    @Override
    public void booleanScalarChange(BooleanScalarEvent e)
    {
        setBoolValue(e.getValue());
    }

    @Override
    public void stateChange(AttributeStateEvent evt)
    {
    }

    @Override
    public void errorChange(ErrorEvent evt)
    {
        if (!getText().equalsIgnoreCase(NO_VALUE))
            setText(NO_VALUE);
        if (!getBackground().equals(falseBackground))
            setBackground(falseBackground);       
    }

    // ---------------------------------------------------
    // Main test fucntion
    // ---------------------------------------------------
    static public void main(String args[])
    {
        IEntity ie;
        IBooleanScalar bAtt;
        AttributeList attl = new AttributeList();
        JFrame f = new JFrame();
        BooleanScalarTickViewer bstv = new BooleanScalarTickViewer();
//        bstv.setHasToolTip(true);
//        bstv.setFont(new Font("Dialog", Font.BOLD, 32));
//        bstv.setFalseBackground(Color.red);


        try
        {
            ie = attl.add("sys/tango-test/lab/boolean_scalar");
            if (!(ie instanceof IBooleanScalar))
            {
                System.out.println("sys/tango-test/lab/boolean_scalar is not a booleanScalar");
                System.exit(0);
            }

            bAtt = (IBooleanScalar) ie;
            bstv.setAttModel(bAtt);
        }
        catch (Exception ex)
        {
            System.out.println("Cannot connect to sys/tango-test/lab/boolean_scalar");
        }

        attl.startRefresher();
        f.setContentPane(bstv);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.pack();
        f.setVisible(true);
    }

}
