package fr.esrf.tangoatk.widget.util.jdraw;

/** JDrawable component info */
public class JDrawableInfo {
  public String className;
  public String name;
  JDrawableInfo(String name,String className) {
    this.className = className;
    this.name = name;
  }
}
