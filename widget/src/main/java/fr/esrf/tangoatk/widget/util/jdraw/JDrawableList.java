/*
 *  Copyright (C) :	2002,2003,2004,2005,2006,2007,2008,2009
 *			European Synchrotron Radiation Facility
 *			BP 220, Grenoble 38043
 *			FRANCE
 * 
 *  This file is part of Tango.
 * 
 *  Tango is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Tango is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Tango.  If not, see <http://www.gnu.org/licenses/>.
 */
 
package fr.esrf.tangoatk.widget.util.jdraw;

import java.util.Vector;

/** A class to add JDrawable JDSwingObject to the editor.
 * @see JDrawEditorFrame#main
 * */
public class JDrawableList {

  // ---------------------------------------------------------
  // initialise default JDrawable list for the JDrawEditorFrame.
  // ---------------------------------------------------------
  static private void init() {

    if(!inited) {

      drawableList.add(new JDrawableInfo("Wheel Switch","fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor"));
      drawableList.add(new JDrawableInfo("Scalar Viewer","fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer"));
      drawableList.add(new JDrawableInfo("String Editor","fr.esrf.tangoatk.widget.attribute.StringScalarEditor"));
      drawableList.add(new JDrawableInfo("Status Viewer","fr.esrf.tangoatk.widget.attribute.StatusViewer"));
      drawableList.add(new JDrawableInfo("Spectrum Plot","fr.esrf.tangoatk.widget.attribute.NumberSpectrumViewer"));
      drawableList.add(new JDrawableInfo("Image Viewer","fr.esrf.tangoatk.widget.attribute.NumberImageViewer"));
      drawableList.add(new JDrawableInfo("Command Button","fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer"));
      drawableList.add(new JDrawableInfo("CheckBox","fr.esrf.tangoatk.widget.attribute.BooleanScalarCheckBoxViewer"));
      drawableList.add(new JDrawableInfo("Number ComboBox","fr.esrf.tangoatk.widget.attribute.NumberScalarComboEditor"));
      drawableList.add(new JDrawableInfo("String ComboBox","fr.esrf.tangoatk.widget.attribute.StringScalarComboEditor"));
      drawableList.add(new JDrawableInfo("Enum ComboBox","fr.esrf.tangoatk.widget.attribute.EnumScalarComboEditor"));
      drawableList.add(new JDrawableInfo("Enum Viewer","fr.esrf.tangoatk.widget.attribute.SimpleEnumScalarViewer"));
      drawableList.add(new JDrawableInfo("Number Viewer","fr.esrf.tangoatk.widget.attribute.DigitalNumberScalarViewer"));
      drawableList.add(new JDrawableInfo("Multi Spectrum Plot","fr.esrf.tangoatk.widget.attribute.MultiNumberSpectrumViewer"));
      drawableList.add(new JDrawableInfo("Trend Plot","fr.esrf.tangoatk.widget.attribute.Trend"));
      drawableList.add(new JDrawableInfo("XY Spectrum Plot (nx2 Image)","fr.esrf.tangoatk.widget.attribute.DualSpectrumImageViewer"));


      inited=true;
    }

  }

  /**
   * Add a JDrawable to the editor, It must be called before the
   * JDrawEditor frame is constructed.
   * @param name Display name of object to add.
   * @param className JDrawable object to add.
   * @see JDrawEditorFrame#main
   */
  static public void addClass(String name,String className) {
    init();
    drawableList.add(new JDrawableInfo(name,className));
  }

  /**
   * @return the list of drawable object known by the editor.
   */
  static public JDrawableInfo[] getDrawalbeList() {
    init();
    JDrawableInfo[] ret = new JDrawableInfo[drawableList.size()];
    for(int i=0;i<drawableList.size();i++)
      ret[i] = drawableList.get(i);
    return ret;
  }

  /** Return user-friendly name of the given className */
  static public String getNameForClass(String className) {
    init();
    boolean found = false;
    int i = 0;
    while(!found && i<drawableList.size()) {
      found = drawableList.get(i).className.equals(className);
      if(!found) i++;
    }
    if(!found)
      return "";
    else
      return drawableList.get(i).name;
  }

  private static Vector<JDrawableInfo> drawableList = new Vector();
  private static boolean inited = false;

}
