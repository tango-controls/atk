/*
 *  Copyright (C) :	2002,2003,2004,2005,2006,2007,2008,2009
 *			European Synchrotron Radiation Facility
 *			BP 220, Grenoble 38043
 *			FRANCE
 *
 *  This file is part of Tango.
 *
 *  Tango is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Tango is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Tango.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * StringScalarEditor.java
 *
 * Created on July 29, 2003, 11:00 AM
 */

/**
 * @author poncet
 */
package fr.esrf.tangoatk.widget.attribute;


import java.awt.*;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IStringScalar;
import fr.esrf.tangoatk.core.IStringScalarListener;
import fr.esrf.tangoatk.core.StringScalarEvent;
import fr.esrf.tangoatk.widget.util.jdraw.JDrawable;

public class StringScalarEditor extends JTextField
        implements IStringScalarListener, JDrawable {
  private IStringScalar model;
  private String lastSet;
  private String errorText = "Read Error";
  private boolean updateImmediately = false;
  static String[] exts = {"invalidText","updateImmediately"};

  /** Creates new form StringScalarEditor */
  public StringScalarEditor() {
    model = null;
    lastSet = null;
    this.addActionListener(
                    new java.awt.event.ActionListener() {
                      public void actionPerformed(java.awt.event.ActionEvent evt) {
                        // User press enter
                        textInsertActionPerformed();
                      }
                    });

    getDocument().addDocumentListener(
            new DocumentListener() {
              public void changedUpdate(DocumentEvent e) {
                if(updateImmediately)
                  textInsertActionPerformed();
              }
              public void removeUpdate(DocumentEvent e) {
                if(updateImmediately)
                  textInsertActionPerformed();
              }
              public void insertUpdate(DocumentEvent e) {
                if(updateImmediately)
                  textInsertActionPerformed();
              }
            });

    setMargin(new Insets(0, 0, 0, 0)); // text will have the maximum available space
  }

  public void setModel(IStringScalar is) {

    // Remove old registered listener
    if (model != null)
      model.removeStringScalarListener(this);

    if (is == null) {
      model = null;
      return;
    }

    if (!is.isWritable())
      throw new IllegalArgumentException("StringScalarEditor: Only accept writeable attribute.");

    model = is;

    // Register new listener
    model.addStringScalarListener(this);
    model.refresh();

    String textFieldString = model.getString();

    if (textFieldString == null)
      textFieldString = "NULL";

    setText(textFieldString);
    lastSet = textFieldString;

  }


  public IStringScalar getModel() {
    return model;
  }


  // Listen on "setpoint" change
  // this is not clean yet as there is no setpointChangeListener
  // Listen on valueChange and readSetpoint
  public void stringScalarChange(StringScalarEvent e) {

    String set = null;

    long now = System.currentTimeMillis();

    //avoiding NullPointerException
    if (model != null) {
      if (hasFocus())
        set = model.getStringDeviceSetPoint();
      else
        set = model.getStringSetPoint();
    }

    if (set != null) {
      // Dont update if the set point has not changed
      if (!set.equals(lastSet)) {
        this.setText(set);
        lastSet = set;
      }
    } else //set == null
    {
      this.setText("NULL");
      lastSet = "NULL";
    }
  }

  public void setErrorText(String text) {
    errorText = text;
  }

  public String getErrorText() {
    return errorText;
  }

  public void errorChange(ErrorEvent e) {
    setText(errorText);
    lastSet = errorText;
  }

  public void stateChange(AttributeStateEvent e) {
  }


  private void textInsertActionPerformed() {
    lastSet = this.getText();
    //avoiding NullPointerException
    if (model != null) {
      model.setString(lastSet);
    }
  }


  public static void main(String[] args) {
    fr.esrf.tangoatk.core.AttributeList attList = new fr.esrf.tangoatk.core.AttributeList();
    StringScalarEditor sse = new StringScalarEditor();
    IStringScalar att;
    JFrame mainFrame;

    // Connect to a "writable" string scalar attribute
    try {
      att = (IStringScalar) attList.add("dev/test/10/String_attr_w");
      sse.setModel(att);
    } catch (Exception ex) {
      System.out.println("caught exception : " + ex.getMessage());
      System.exit(-1);
    }

    attList.startRefresher();

    mainFrame = new JFrame();

    mainFrame.getContentPane().add(sse);
    mainFrame.pack();

    mainFrame.setVisible(true);

  } // end of main ()

  @Override
  public void initForEditing() {
    setText("");
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public String[] getExtensionList() {
    return exts;
  }

  @Override
  public boolean setExtendedParam(String name, String value, boolean popupAllowed) {
    if (name.equalsIgnoreCase("invalidText")) {
      setErrorText(value);
      return true;
    } else if( name.equalsIgnoreCase("updateImmediately") ) {
      updateImmediately = value.equalsIgnoreCase("true")?true:false;
    }
    return false;
  }

  public String getExtendedParam(String name) {
    if(name.equals("invalidText")) {
      return getErrorText();
    } else if( name.equalsIgnoreCase("updateImmediately") ) {
      return updateImmediately ? "true":"false";
    }
    return "";
  }

  @Override
  public String getDescription(String extName) {
    if (extName.equalsIgnoreCase("invalidText")) {
      return "Text displayed when the qulaity factor is INVALID\n" +
              "or when the connection is lost.";
    }  else if( extName.equalsIgnoreCase("updateImmediately") ) {
      return "Write immediately to device on text change";
    }
    return "";
  }

}

