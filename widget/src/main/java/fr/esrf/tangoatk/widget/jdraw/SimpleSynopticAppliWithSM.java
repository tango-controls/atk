package fr.esrf.tangoatk.widget.jdraw;

public class SimpleSynopticAppliWithSM extends SimpleSynopticAppli {

  /**
   * Creates a SimpleSynopticAppli with setting manager that takes file_name,settings_manager_name as input
   */

  public SimpleSynopticAppliWithSM(String param) {

    String[] params = param.split(",");
    if(params.length!=2) {
      javax.swing.JOptionPane.showMessageDialog(
              null, "SimpleSynopticAppliWithSM input param must be: file_name,settings_manager_name",
              "SimpleSynopticAppliWithSM Error",
              javax.swing.JOptionPane.ERROR_MESSAGE);
      return;
    }

    init(params[0], params[1], false);
    pack();
    setVisible(true);

  }

}
