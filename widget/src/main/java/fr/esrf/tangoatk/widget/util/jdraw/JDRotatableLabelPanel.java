/*
 *  Copyright (C) :	2002,2003,2004,2005,2006,2007,2008,2009
 *			European Synchrotron Radiation Facility
 *			BP 220, Grenoble 38043
 *			FRANCE
 *
 *  This file is part of Tango.
 *
 *  Tango is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Tango is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Tango.  If not, see <http://www.gnu.org/licenses/>.
 */

/** A panel for JDRotatableLabelPanel private properties */
package fr.esrf.tangoatk.widget.util.jdraw;

import fr.esrf.tangoatk.widget.util.ATKFontChooser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class JDRotatableLabelPanel extends JPanel implements ActionListener {

  private JScrollPane textView;
  private JTextArea textText;
  private JButton applyTextBtn;

  private JLabel fontLabel;
  private JButton fontBtn;

  private JLabel angleLabel;
  private JTextField angleText;

  private JDRotatableLabel allObjects[] = null;
  private JDrawEditor invoker;
  private Rectangle oldRect;
  private boolean isUpdating = false;

  public JDRotatableLabelPanel(JDRotatableLabel[] p, JDrawEditor jc) {

    invoker = jc;
    setLayout(null);
    setBorder(BorderFactory.createEtchedBorder());
    setPreferredSize(new Dimension(380, 290));

    // ------------------------------------------------------------------------------------
    JPanel namePanel = new JPanel(null);
    namePanel.setBorder(JDUtils.createTitleBorder("Text"));
    namePanel.setBounds(5,5,370,130);

    textText = new JTextArea();
    textText.setEditable(true);
    textText.setFont(JDUtils.labelFont);
    textView = new JScrollPane(textText);
    textView.setBounds(10, 20, 350, 70);
    namePanel.add(textView);

    applyTextBtn = new JButton("Apply");
    applyTextBtn.setFont(JDUtils.labelFont);
    applyTextBtn.addActionListener(this);
    applyTextBtn.setBounds(260,95,100,25);
    namePanel.add(applyTextBtn);

    add(namePanel);

    // ------------------------------------------------------------------------------------
    JPanel stylePanel = new JPanel(null);
    stylePanel.setBorder(JDUtils.createTitleBorder("Text styles"));
    stylePanel.setBounds(5,140,370,145);

    fontLabel = new JLabel("Font");
    fontLabel.setFont(JDUtils.labelFont);
    fontLabel.setForeground(JDUtils.labelColor);
    fontLabel.setBounds(10, 20, 135, 24);
    stylePanel.add(fontLabel);

    fontBtn = new JButton();
    fontBtn.setText("Choose");
    fontBtn.setMargin(new Insets(0, 0, 0, 0));
    fontBtn.setFont(JDUtils.labelFont);
    fontBtn.setBounds(220, 20, 140, 24);
    fontBtn.addActionListener(this);
    stylePanel.add(fontBtn);


    angleLabel = new JLabel("Text rotation");
    angleLabel.setFont(JDUtils.labelFont);
    angleLabel.setForeground(JDUtils.labelColor);
    angleLabel.setBounds(10, 50, 100, 25);
    stylePanel.add(angleLabel);

    angleText = new JTextField("");
    angleText.setFont(JDUtils.labelFont);
    angleText.setEditable(true);
    angleText.addActionListener(this);
    angleText.setBounds(110, 50, 100, 25);
    stylePanel.add(angleText);

    add(stylePanel);

    updatePanel(p);

  }

  public void updatePanel(JDRotatableLabel[] objs) {

    allObjects = objs;
    isUpdating = true;

    if (objs == null || objs.length <= 0) {

      textText.setText("");
      fontLabel.setText("Font: ");
      angleText.setText("");

    } else {

      JDRotatableLabel p = objs[0];

      fontLabel.setText("Font: [" + JDUtils.buildFontName(p.getFont())+ "]");
      textText.setText(p.getText());
      angleText.setText( String.format("%.2f",Math.toDegrees(p.getAngle())) );

    }

    isUpdating = false;

  }

  private void initRepaint() {
    if(allObjects==null) return;
    oldRect = allObjects[0].getRepaintRect();
    for (int i = 1; i < allObjects.length; i++)
      oldRect = oldRect.union(allObjects[i].getRepaintRect());
  }

  private void repaintObjects() {
    if(allObjects==null) return;
    Rectangle newRect = allObjects[0].getRepaintRect();
    for (int i = 1; i < allObjects.length; i++)
      newRect = newRect.union(allObjects[i].getRepaintRect());
    invoker.repaint(newRect.union(oldRect));
  }

  // ---------------------------------------------------------
  // Action listener
  // ---------------------------------------------------------
  public void actionPerformed(ActionEvent e) {

    if(allObjects==null || isUpdating) return;

    int i;
    initRepaint();
    Object src = e.getSource();
    if (src == fontBtn) {
      Font newFont = ATKFontChooser.getNewFont(this,"Choose label font",allObjects[0].getFont());
      if (newFont != null) {
        for (i = 0; i < allObjects.length; i++)
          allObjects[i].setFont(newFont,invoker.resizeLabelOnFontChange);
        fontLabel.setText("Font: [" + JDUtils.buildFontName(newFont) + "]");
        invoker.setNeedToSave(true,"Change font");
      }
    } else if (src == angleText) {
      double angle = Math.toRadians(Double.parseDouble(angleText.getText()));
      for (i = 0; i < allObjects.length; i++)
        allObjects[i].setAngle(angle);
      invoker.setNeedToSave(true,"Change angle");
    } else if (src == applyTextBtn ) {
      for (i = 0; i < allObjects.length; i++)
        allObjects[i].setText(textText.getText(),invoker.resizeLabelOnTextChange);
      invoker.setNeedToSave(true,"Change text");
    }

    repaintObjects();
  }


}
